select tb.* from 
(
	select article_tic,
		color_tics,
		DATE_FORMAT(creation_date, '%Y-%m-%dT%TZ') as creation_date,
		creation_user,
		field_tics,
		historic_tics,
		is_actual_value,
		parent_historic_tics,
		size_tics,
		value,
		value_tics
	from articles_historic 
	where 
		article_tic = 1 and 
		field_tics in (true) and
		historic_tics in (true) and
		parent_historic_tics in (true) and
		color_tics in (true) and 
		size_tics in (true) and
		DATE_FORMAT(creation_date, '%Y-%m-%dT%TZ') like '2021-01-10T17:31:49Z' and
		value_tics in (true) and
		value like 'Joe' and
		is_actual_value is true and
		creation_user like 'Kate'
	limit 1000
) as tb
order by parent_historic_tics asc;