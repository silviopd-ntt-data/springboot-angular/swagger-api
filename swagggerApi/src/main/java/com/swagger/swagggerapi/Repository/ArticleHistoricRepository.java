package com.swagger.swagggerapi.Repository;

import com.swagger.swagggerapi.Entity.ArticleHistoric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Objects;


@Repository
public interface ArticleHistoricRepository extends JpaRepository<ArticleHistoric, Long> {

    @Query(value = "select tb.* from " +
            "( " +
            " select id," +
            " article_tic, " +
            " color_tics, " +
            " creation_date, " +
            " creation_user, " +
            " field_tics, " +
            " historic_tics, " +
            " is_actual_value, " +
            " parent_historic_tics, " +
            " size_tics, " +
            " value, " +
            " value_tics " +
            " from articles_historic " +
            " where " +
            " article_tic = ?1 and " +
            " field_tics in (?2) and " +
            " historic_tics in (?3) and " +
            " parent_historic_tics in (?4) and " +
            " color_tics in (?5) and " +
            " size_tics in (?6) and " +
            " value_tics in (?7) and " +
            " value like ?8 and " +
            " is_actual_value in (?9) and " +
            " creation_user like ?10 " +
            " limit 1000 " +
            ") as tb " +
            "order by parent_historic_tics asc;", nativeQuery = true)
    public List<ArticleHistoric> busquedaArticleHistoric(int articleTic, String fieldTics, Object historicTics, Object parentHistoricTics, Object colorTics, Object sizeTics, Object valueTics, String value, Boolean isActualValue, String creationUser);


}
