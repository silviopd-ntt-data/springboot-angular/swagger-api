package com.swagger.swagggerapi.Controller;

import com.swagger.swagggerapi.Dto.ArticHistoricRequestDTO;
import com.swagger.swagggerapi.Dto.ArticleHistoricResponseDTO;
import com.swagger.swagggerapi.Dto.ErrorDTO;
import com.swagger.swagggerapi.Service.ArticleHistoricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v2/articles/historics")
@CrossOrigin(origins = "*")
public class ArticleHistoricController {

    @Autowired
    ArticleHistoricService articleHistoricService;

    @PostMapping
    public ResponseEntity<?> getArticleHistoric(@Valid @RequestBody ArticHistoricRequestDTO articHistoricRequestDTO) {
        System.out.println(articHistoricRequestDTO);
        try {
            return ArticleHistoricResponseDTO.responseArticleHistoric(articleHistoricService.listar(articHistoricRequestDTO));
        } catch (Exception e) {
            return ErrorDTO.responseError(400, e.getMessage(), "id", "INVALID:MQS", "Inconsistency in mq parameter. Make sure you use the pattern model-quality in a consistent way");
        }
    }
}
