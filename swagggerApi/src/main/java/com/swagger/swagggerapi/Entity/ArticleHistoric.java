package com.swagger.swagggerapi.Entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "articles_historic")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ArticleHistoric {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int articleTic, fieldTics, colorTics, sizeTics;
    private long historicTics, parentHistoricTics, valueTics;

    private Date creationDate;

    private String value, creationUser;
    private boolean isActualValue;

}
