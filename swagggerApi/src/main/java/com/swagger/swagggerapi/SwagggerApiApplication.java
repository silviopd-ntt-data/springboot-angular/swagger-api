package com.swagger.swagggerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwagggerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwagggerApiApplication.class, args);
    }

}
