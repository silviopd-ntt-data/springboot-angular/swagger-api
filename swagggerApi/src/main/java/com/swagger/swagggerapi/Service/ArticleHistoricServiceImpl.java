package com.swagger.swagggerapi.Service;

import com.swagger.swagggerapi.Dto.ArticHistoricRequestDTO;
import com.swagger.swagggerapi.Dto.ArticleHistoricDTO;
import com.swagger.swagggerapi.Dto.ArticleHistoricsDTO;
import com.swagger.swagggerapi.Entity.ArticleHistoric;
import com.swagger.swagggerapi.Repository.ArticleHistoricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ArticleHistoricServiceImpl implements ArticleHistoricService {

    @Autowired
    private ArticleHistoricRepository articleHistoricRepository;

    @Override
    public List<ArticleHistoricsDTO> listar(ArticHistoricRequestDTO articHistoricRequestDTO) {


        int articleTic = articHistoricRequestDTO.getArticleTic();
        String fieldTics = String.valueOf(fnArrayIntSql(articHistoricRequestDTO.getFieldTics()));
        Object historicTics = articHistoricRequestDTO.getHistoricTics().length > 0 ? fnArrayLongSql(articHistoricRequestDTO.getHistoricTics()) : Boolean.TRUE;
        Object parentHistoricTics = articHistoricRequestDTO.getParentHistoricTics().length > 0 ? fnArrayLongSql(articHistoricRequestDTO.getParentHistoricTics()) : Boolean.TRUE;
        Object colorTics = articHistoricRequestDTO.getColorTics().length > 0 ? fnArrayIntSql(articHistoricRequestDTO.getColorTics()) : Boolean.TRUE;
        Object sizeTics = articHistoricRequestDTO.getSizeTics().length > 0 ? fnArrayIntSql(articHistoricRequestDTO.getSizeTics()) : Boolean.TRUE;

        //buscar manipulacion de fecha
//        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        String creationDate = formatoFecha.format(articHistoricRequestDTO.getCreationDate());

        Object valueTics = articHistoricRequestDTO.getValueTics().length > 0 ? fnArrayLongSql(articHistoricRequestDTO.getValueTics()) : Boolean.TRUE;
        String value = articHistoricRequestDTO.getValue().length() > 0 ? articHistoricRequestDTO.getValue() : "%";

        Boolean isActualValue = articHistoricRequestDTO.isActualValue();
        String creationUser = articHistoricRequestDTO.getCreationUser().length() > 0 ? articHistoricRequestDTO.getCreationUser() : "%";

        System.out.println("articleTic: " + articleTic);
        System.out.println("fieldTics: " + fieldTics);
        System.out.println("historicTics: " + historicTics);
        System.out.println("parentHistoricTics: " + parentHistoricTics);
        System.out.println("colorTics: " + colorTics);
        System.out.println("sizeTics: " + sizeTics);
        System.out.println("valueTics: " + valueTics);
        System.out.println("value: " + value);
        System.out.println("isActualValue: " + isActualValue);
        System.out.println("creationUser: " + creationUser);

        //Datos
        List<ArticleHistoric> listArticleHistoric = articleHistoricRepository.busquedaArticleHistoric(
                articleTic,
                fieldTics,
                historicTics,
                parentHistoricTics,
                colorTics,
                sizeTics,
//                creationDate,
                valueTics,
                value,
                isActualValue,
                creationUser
        );


        //Lo que quiero retornar
        List<ArticleHistoricsDTO> listArticleHistoricsDTO = new ArrayList<>();

        //indice para la busqueda de los articulos con los padres
        int indexArticle = 0;

        for (int i = 0; i < listArticleHistoric.size(); i++) {

            if (i == 0) {
                //Lista de articulos
                List<ArticleHistoricDTO> listArticleHistoricDTO = new ArrayList<>();

                //LLenado de los articulos
//                fnArticleHistoricDTO(listArticleHistoric.get(i));

                //agregar articulo a la lista
                listArticleHistoricDTO.add(fnArticleHistoricDTO(listArticleHistoric.get(i)));

                //agregando padre de los articulos
                ArticleHistoricsDTO articleHistoricsDTO = new ArticleHistoricsDTO();
                articleHistoricsDTO.setParentHistoricTics(listArticleHistoric.get(i).getParentHistoricTics());
                articleHistoricsDTO.setArticleHistoricDTOs(listArticleHistoricDTO);

                //agregando lo que quiero retornar al padre de los articulos
                listArticleHistoricsDTO.add(articleHistoricsDTO);
            } else {

                if (listArticleHistoricsDTO.get(indexArticle).getParentHistoricTics() == listArticleHistoric.get(i).getParentHistoricTics()) {
                    listArticleHistoricsDTO.get(indexArticle).getArticleHistoricDTOs().add(fnArticleHistoricDTO(listArticleHistoric.get(i)));
                } else {
                    //Lista de articulos
                    List<ArticleHistoricDTO> listArticleHistoricDTO = new ArrayList<>();

                    //agregar articulo a la lista
                    listArticleHistoricDTO.add(fnArticleHistoricDTO(listArticleHistoric.get(i)));

                    //agregando padre de los articulos
                    ArticleHistoricsDTO articleHistoricsDTO = new ArticleHistoricsDTO();
                    articleHistoricsDTO.setParentHistoricTics(listArticleHistoric.get(i).getParentHistoricTics());
                    articleHistoricsDTO.setArticleHistoricDTOs(listArticleHistoricDTO);

                    //agregando lo que quiero retornar al padre de los articulos
                    listArticleHistoricsDTO.add(articleHistoricsDTO);

                    //subimos el indice de los articulos
                    indexArticle++;
                }
            }
        }

        return listArticleHistoricsDTO;

    }

    private ArticleHistoricDTO fnArticleHistoricDTO(ArticleHistoric v_articleHistoricDTO) {
        ArticleHistoricDTO articleHistoricDTO = new ArticleHistoricDTO();
        articleHistoricDTO.setArticleTic(v_articleHistoricDTO.getArticleTic());
        articleHistoricDTO.setFieldTics(v_articleHistoricDTO.getFieldTics());
        articleHistoricDTO.setHistoricTics(v_articleHistoricDTO.getHistoricTics());
        articleHistoricDTO.setColorTics(v_articleHistoricDTO.getColorTics());
        articleHistoricDTO.setSizeTics(v_articleHistoricDTO.getSizeTics());
        articleHistoricDTO.setCreationDate(String.valueOf(v_articleHistoricDTO.getCreationDate()));
        articleHistoricDTO.setValueTics(v_articleHistoricDTO.getValueTics());
        articleHistoricDTO.setValue(v_articleHistoricDTO.getValue());
        articleHistoricDTO.setActualValue(v_articleHistoricDTO.isActualValue());
        articleHistoricDTO.setCreationUser(v_articleHistoricDTO.getCreationUser());

        return articleHistoricDTO;
    }

    private Object fnArrayIntSql(int[] value) {

        String result = "";

        if (value.length == 0) {
            return Boolean.TRUE;
        }

        for (int i = 0; i < value.length; i++) {
            if (i == 0) {
                result += value[i];
            } else {
                result += "," + value[i];
            }
        }

        return result;
    }

    private Object fnArrayLongSql(long[] value) {

        String result = "";

        if (value.length == 0) {
            return Boolean.TRUE;
        }

        for (int i = 0; i < value.length; i++) {
            if (i == 0) {
                result += value[i];
            } else {
                result += "," + value[i];
            }
        }

        return result;
    }
}
