package com.swagger.swagggerapi.Service;

import com.swagger.swagggerapi.Dto.ArticHistoricRequestDTO;
import com.swagger.swagggerapi.Dto.ArticleHistoricDTO;
import com.swagger.swagggerapi.Dto.ArticleHistoricsDTO;
import com.swagger.swagggerapi.Entity.ArticleHistoric;

import java.util.List;

public interface ArticleHistoricService {

    public List<ArticleHistoricsDTO> listar(ArticHistoricRequestDTO articHistoricRequestDTO);
}
