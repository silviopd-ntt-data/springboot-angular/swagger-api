package com.swagger.swagggerapi.Dto;

import lombok.Data;

import java.util.List;

@Data
public class ArticleHistoricsDTO {

    private long parentHistoricTics;
    private List<ArticleHistoricDTO> articleHistoricDTOs;
}
