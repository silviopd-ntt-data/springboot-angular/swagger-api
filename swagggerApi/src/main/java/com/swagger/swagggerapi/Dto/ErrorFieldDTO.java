package com.swagger.swagggerapi.Dto;

import lombok.Data;

@Data
public class ErrorFieldDTO {

    private String field, errorFieldKey, description;
}
