package com.swagger.swagggerapi.Dto;

import lombok.Data;
import org.springframework.http.ResponseEntity;

@Data
public class ErrorDTO {

    private String message;
    private ErrorFieldDTO errorFieldDTO;
    private Integer status;

    public static ResponseEntity<?> responseError(Integer status, String message, String field, String errorFieldKey, String description) {

        ErrorFieldDTO errorFieldDTO = new ErrorFieldDTO();
        errorFieldDTO.setField(field);
        errorFieldDTO.setErrorFieldKey(errorFieldKey);
        errorFieldDTO.setDescription(description);

        ErrorDTO responseDto = new ErrorDTO();

        responseDto.setMessage(message);
        responseDto.setErrorFieldDTO(errorFieldDTO);
        responseDto.setStatus(status);

        return ResponseEntity.status(responseDto.getStatus()).body(responseDto);
    }
}
