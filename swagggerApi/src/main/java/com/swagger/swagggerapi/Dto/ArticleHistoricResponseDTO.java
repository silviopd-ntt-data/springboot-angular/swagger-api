package com.swagger.swagggerapi.Dto;

import com.swagger.swagggerapi.Entity.ArticleHistoric;
import lombok.Data;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@Data
public class ArticleHistoricResponseDTO {

    private List<ArticleHistoricsDTO> articleHistoricsDTOs = new ArrayList<>();

    public static ResponseEntity<?> responseArticleHistoric(List<ArticleHistoricsDTO> articleHistoricsDTO) {

        ArticleHistoricResponseDTO responseDto = new ArticleHistoricResponseDTO();
        responseDto.setArticleHistoricsDTOs(articleHistoricsDTO);

        return ResponseEntity.ok(responseDto);
    }
}
