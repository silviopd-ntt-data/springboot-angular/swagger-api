package com.swagger.swagggerapi.Dto;

import lombok.Data;

import java.sql.Date;

@Data
public class ArticleHistoricDTO {

    private int articleTic;
    private int fieldTics;
    private long historicTics;
    private int colorTics;
    private int sizeTics;
    private String creationDate;
    private long valueTics;
    private String value;
    private boolean isActualValue;
    private String creationUser;
}
